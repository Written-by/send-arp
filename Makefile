all: send-arp

send-arp: main.o arphdr.o ethhdr.o ip.o mac.o
	g++ -o send-arp main.o arphdr.o ethhdr.o ip.o mac.o -lpcap

main.o: main.cpp mac.h ip.h ethhdr.h arphdr.h
	g++ -c -o main.o main.cpp

arphdr.o: mac.h ip.h arphdr.h arphdr.cpp
	g++ -c -o arphdr.o arphdr.cpp

ethhdr.o: mac.h ethhdr.h ethhdr.cpp
	g++ -c -o ethhdr.o ethhdr.cpp

ip.o: ip.h ip.cpp
	g++ -c -o ip.o ip.cpp

mac.o: mac.h mac.cpp
	g++ -c -o mac.o mac.cpp

clean:
	rm -f *.o
	rm -f send-arp
